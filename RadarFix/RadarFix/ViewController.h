//
//  ViewController.h
//  RadarFix
//
//  Created by YYY on 2017/12/8.
//  Copyright © 2017年 成品家（北京）网路科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XHRadarView.h"

@interface ViewController : UIViewController <XHRadarViewDataSource, XHRadarViewDelegate>

@property (nonatomic, strong) XHRadarView *radarView;

@end

